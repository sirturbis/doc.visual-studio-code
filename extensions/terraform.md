# Terraform
Syntax highlighting, linting, formatting, and validation for Hashicorp's Terraform
* [HomePage](https://marketplace.visualstudio.com/items?itemName=mauve.terraform)

## Installation
* Launch VS Code Quick Open (Ctrl+P), paste `ext install mauve.terraform`, and press enter.

## Enable HCLv2 support
* Launch VS Code Quick Command Pannel (Ctrl+Shift+P), paste `Terraform: Install`, and press enter after that choose the version and press enter.
* Launch VS Code Quick Command Pannel (Ctrl+Shift+P), paste `Terraform: Enable`, and press enter after that choose the version and press enter.
