# Indent-rainbow
This extension colorizes the indentation in front of your text alternating four different colors on each step. Some may find it helpful in writing code for Nim or Python.
* [HomePage](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow)

## Installation
* Launch VS Code Quick Open (Ctrl+P), paste `ext install oderwat.indent-rainbow`, and press enter.
