# Material Icon Theme
Material Design Icons for Visual Studio Code

* [HomePage](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)

## Installation
* Launch VS Code Quick Open (Ctrl+P), paste `ext install PKief.material-icon-theme`, and press enter.
