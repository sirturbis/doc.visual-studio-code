# Bracket Pair Colorizer 2
This extension allows matching brackets to be identified with colours. The user can define which tokens to match, and which colours to use.
* [HomePage](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

## Installation
* Launch VS Code Quick Open (Ctrl+P), paste `ext install CoenraadS.bracket-pair-colorizer-2`, and press enter.
