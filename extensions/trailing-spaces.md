# Trailing Spaces
Highlight trailing spaces and delete them in a flash!
* [HomePage](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces)

## Installation
* Launch VS Code Quick Open (Ctrl+P), paste `ext install shardulm94.trailing-spaces`, and press enter.

