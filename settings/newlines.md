# Trim Trailling new lines
* Press [ctrl+,] to Settings
* Search `Trim Final Newlines` in top search bar.
* Select `Trim Final Newlines`

# Add new line to end of file
* Press [ctrl+,] to Settings
* Search `Insert final newline` in top search bar.
* Select `Insert final newline`
