# Visual Studio Code

## Installation
See the [Download Visual Studio Code page](https://code.visualstudio.com/download) for a complete list of available installation options.

## Settings
* [White spaces](settings/whitespaces.md)
* [New lines](settings/newlines.md)

## Extensions
* [Trailing Spaces](extensions/trailing-spaces.md)
* [Material Icon](extensions/material-icon-theme.md)
* [Bracket Pair Colorizer](extensions/bracket-pair-colorizer.md)
* [Indent Color](extensions/indent-color.md)
* [Terraform](extensions/terraform.md)

## Workflows

## Cheat Sheets
* [Common](cheatsheets/common.md)

